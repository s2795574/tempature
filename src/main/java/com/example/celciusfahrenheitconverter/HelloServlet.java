package com.example.celciusfahrenheitconverter;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private Converter converter;

    public void init() {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#EE9E64\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P> Celsius: " +
                request.getParameter("degrees") + "\n" +
                "  <P>Fahrenheit: " +
                Double.toString(converter.getFahrenheit(request.getParameter("degrees"))) +
                "</BODY></HTML>");
    }

    public void destroy() {
    }
}