package com.example.celciusfahrenheitconverter;

public class Converter {
    public double getFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * 1.8) + 32;
    }
}
